package implementsRunnable;

import extendsThread.PrintChar;
import extendsThread.PrintNum;

public class MainApp
{
    public static void main(String[] args) {
        //Extra step when using implements runnable
        //Need to wrap the new class in a thread
        Thread printA = new Thread(new PrintChar('A', 100));
        Thread printB = new Thread(new PrintChar('B', 100));
        Thread print100 = new Thread(new PrintNum(100));

        print100.start();
        printA.start();
        printB.start();
    }
}
