package UnfairBankAccount;

public class Account
{
    private double balance;
    public static Object sharedLock = new Object();

    public Account()
    {
        this.balance = 0;
    }

    public Account(double balance)
    {
        this.balance = balance;
    }

    public void deposit(double amount)
    {
        double temp;
        synchronized (sharedLock)
        {
            temp = this.balance;

            try
            {
                Thread.sleep(500);
            } catch (InterruptedException e)
            {
                System.out.println(e.getMessage());
            }

            temp = temp + amount;
            balance = temp;
        }
    }

    public void withdraw(double amount)
    {
        try
        {
            Thread.sleep(500);
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }

        balance -= amount;
    }

    public double getBalance()
    {
        return balance;
    }
}
