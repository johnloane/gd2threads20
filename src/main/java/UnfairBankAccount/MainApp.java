package UnfairBankAccount;

public class MainApp
{
    public static void main(String[] args)
    {
        Account patricksAccount = new Account(1000);

        Runnable runner = new AccountThread(patricksAccount);

        Thread t1 = new Thread(runner);
        Thread t2 = new Thread(runner);
        Thread t3 = new Thread(runner);

        t1.start();
        t2.start();
        t3.start();

        //Wait for the three threads to finish
        try {
            t1.join();
            t2.join();
            t3.join();
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println("Account balance is: " + patricksAccount.getBalance());

    }
}
