package SharedResource;

public class Resource
{
    private boolean conFree;

    public Resource()
    {
        conFree = true;
    }

    public synchronized void getConnection(String name)
    {
        System.out.println(name + " Entered getConnection()");
        while(conFree == false)
        {
            
        }
    }
}
