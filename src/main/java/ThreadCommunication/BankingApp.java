package ThreadCommunication;

public class BankingApp
{
    public static void main(String[] args)
    {
        BankAccount jacksAccount = new BankAccount();

        //create the spender thread
        Spender spender = new Spender(jacksAccount);
        Thread spenderThread = new Thread(spender);

        Saver saver = new Saver(jacksAccount);
        Thread saverThread = new Thread(saver);

        spenderThread.start();
        saverThread.start();

        int time;
        if(args.length == 0)
        {
            time = 10000;
        }
        else
        {
            time = Integer.parseInt(args[0]) * 1000;
        }
        try
        {
            Thread.currentThread().sleep(time);
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        jacksAccount.close();
    }
}
