package ThreadCommunication;
//Demonstrate wait and notify methods
public class BankAccount
{
    private double balance = 0;
    private boolean isOpen = true;

    /*
    This method withdraws an amount from an account. If the funds are insufficient, it will
    wait until the funds are available or the account is closed
    @param amount The amount to withdraw from the account
    @return true is the withdrawal is successful, false otherwise
    @exception InterruptedException is another thread interrupts
     */

    public synchronized  boolean withdraw(int amount)
    {
        while(amount > balance && isOpen())
        {
            System.out.println("Waiting for some money ....");
            try
            {
                wait();
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }

        boolean result = false;

        if(isOpen())
        {
            balance -= amount;
            result = true;
        }
        return result;
    }

    /**
     * Checks if the account is open
     * @return true if open, false if not
     */
    public synchronized boolean isOpen()
    {
        return isOpen;
    }

    /**
     * Deposits an amount assuming that the account is open
     * When the deposit is successful it will notify one
     * waiting thread at random
     * @param amount to be deposited
     * @return true for successful deposit, otherwise false
     */
    public synchronized boolean deposit(int amount)
    {
        if(isOpen())
        {
            balance += amount;
            notifyAll();
            return true;
        }
        else
        {
            return false;
        }
    }

    public synchronized void close()
    {
        isOpen = false;
        notifyAll();
    }
}
