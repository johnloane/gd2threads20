package ThreadCommunication;

public class Saver implements Runnable
{
    private BankAccount account;

    public Saver(BankAccount ba)
    {
        this.account = ba;
    }

    @Override
    public void run()
    {
        while(account.isOpen())
        {
            if(account.deposit(100))
            {
                System.out.println("€100 successfully deposited");
            }
            try
            {
                Thread.currentThread().sleep(1000);
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
}
